import React from 'react';
import { HexGrid } from '../hex/hex-grid/HexGrid';

export function Game() {
  return (
      <HexGrid />
  );
}
