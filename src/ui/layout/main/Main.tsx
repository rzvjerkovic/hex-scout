import React from 'react';
import './Main.css';
import { Game } from '../../game/Game';

import { BrowserRouter, Link, Switch, Route } from 'react-router-dom';

export function Main() {
    return (
        <div className="main">
            <BrowserRouter>
                <div className="sidebar">
                    <div>
                        <nav>
                            <ul>
                                <li>
                                    <Link to="/game">Game</Link>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div className="topbar">

                </div>
                <div className="content">
                    <Switch>
                        <Route path="/game">
                            <Game></Game>
                        </Route>
                    </Switch>
                </div>
            </BrowserRouter>
        </div>
    );
}
