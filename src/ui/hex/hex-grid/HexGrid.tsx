import React from 'react';

import './HexGrid.css';

interface HexGridProps {
}
export function HexGrid(props: HexGridProps) {
    return (
        <div className="grid">
            {/* TODO: make each tile a component */}
            <div className="tile1" onClick={tileClicked}>
                <div className="hexagon" onClick={hexClicked}>
                    <div className="interactive" onClick={interactiveClicked}>
                        <span onClick={spaClicked}>1</span>
                    </div>
                </div>
            </div>
            <div className="tile2">
                <div className="hexagon">
                    <div className="interactive">
                        <span>2</span>
                    </div>
                </div>
            </div>
            <div className="tile3">
                <div className="hexagon">
                    <div className="interactive">
                        <span>3</span>
                    </div>
                </div>
            </div>
            <div className="tile4">
                <div className="hexagon">
                    <div className="interactive">
                        <span>4</span>
                    </div>
                </div>
            </div>
            <div className="tile5">
                <div className="hexagon">
                    <div className="interactive">
                        <span>5</span>
                    </div>
                </div>
            </div>
            <div className="tile6">
                <div className="hexagon">
                    <div className="interactive">
                        <span>6</span>
                    </div>
                </div>
            </div>
            <div className="tile7">
                <div className="hexagon">
                    <div className="interactive">
                        <span>7</span>
                    </div>
                </div>
            </div>
        </div>
    );
}

function tileClicked() {
    console.log('tileClicked');
}

function hexClicked() {
    console.log('hexClicked');
}

function interactiveClicked() {
    console.log('interactiveClicked');
}

function spaClicked() {
    console.log('spaClicked');
}