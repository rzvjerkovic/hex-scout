import React from 'react';
import './App.css';
import { Main } from './ui/layout/main/Main';

export function App() {
  return (
      <Main />
  );
}
